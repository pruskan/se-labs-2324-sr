def char_types_count(input_string):
    uppercase_count = 0
    lowercase_count = 0
    digit_count = 0

    for char in input_string:
        if char.isupper():
            uppercase_count += 1
        elif char.islower():
            lowercase_count += 1
        elif char.isdigit():
            digit_count += 1

    return (uppercase_count, lowercase_count, digit_count)
def main():
    result = char_types_count("asdf98CXX21grrrr")
    print(result)

if __name__ == "__main__":
    main()