def count_evens(list):
    count = 0


    for number in list:
        if(number % 2 == 0):
            count += 1

    return count

def main():
    print(count_evens([2, 1, 2, 3, 4]))
    print(count_evens([2, 2, 0]))
    print(count_evens([1, 3, 5]))

if __name__ == "__main__":
    main()
