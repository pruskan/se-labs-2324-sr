def centered_average(nums):
    nums.remove(min(nums))
    nums.remove(max(nums))

    average = sum(nums) // len(nums)

    return average

def main():
    print(centered_average([1, 2, 3, 4, 100]))
    print(centered_average([1, 1, 5, 5, 10, 8, 7]))
    print(centered_average([-10, -4, -2, -4, -2, 0]))

if __name__ == "__main__":
    main()
