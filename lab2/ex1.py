def main():
    name = input("Input your name: ")

    print(f"Greetings {name}")
    print("Greetings " + name)
    print("Greetings {}".format(name))

if __name__ == "__main__":
    main()