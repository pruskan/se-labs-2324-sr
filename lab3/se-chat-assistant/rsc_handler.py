from query_handler_base import QueryHandlerBase
import random

class RockPaperScissorsHandler(QueryHandlerBase):
    def can_process_query(self, query):
        return any(word in query.lower() for word in ['rock', 'paper', 'scissors'])

    def process(self, query):
        options = ['rock', 'paper', 'scissors']

        user_choice = self.ui.ask("Choose rock, paper, or scissors:")

        if user_choice.lower() not in options:
            self.ui.query_not_understood()
            return

        computer_choice = random.choice(options)

        self.ui.say(f"birao si {user_choice}.")
        self.ui.say(f"Billy je birao {computer_choice}.")

        if user_choice == computer_choice:
            self.ui.say("Nerješeno")
        elif (user_choice == 'rock' and computer_choice == 'scissors') or \
             (user_choice == 'paper' and computer_choice == 'rock') or \
             (user_choice == 'scissors' and computer_choice == 'paper'):
            self.ui.say("Pobijedio si")
        else:
            self.ui.say("Billy je pobijedio")
