from query_handler_base import QueryHandlerBase
#from pip._vendor import requests
import requests

class TranslateHandler(QueryHandlerBase):
    def can_process_query(self, query):
        # Returns true if the query contains words that this parser is
        # supposed to process
        # Else, returns false
        if "translate" in query:
            return True
        return False

    def process(self, query):
        # Processes the query using the self.ui.say(), self.ui.ask() and
        # self.ui.query_not_understood() methods.
        # Doesn't return a value

        translate_params = query.split()
        result_string = " ".join(translate_params[1:])

        try:
            url = "https://google-translate1.p.rapidapi.com/language/translate/v2"

            payload = {
                "q": f"{result_string}",
                "target": "en",
                "source": "hr"
            }
            headers = {
                "content-type": "application/x-www-form-urlencoded",
                "Accept-Encoding": "application/gzip",
                "X-RapidAPI-Key": "3a30aa888cmsh26f4ffe26648224p114018jsn18c1949bae07",
                "X-RapidAPI-Host": "google-translate1.p.rapidapi.com"
            }

            response = requests.post(url, data=payload, headers=headers)
            data = response.json()
            result = data['data']['translations'][0]['translatedText']

            self.ui.say(result)
        except:
            print("error")