from query_handler_base import QueryHandlerBase
#from pip._vendor import requests
import requests

class MovieHandler(QueryHandlerBase):
    def can_process_query(self, query):
        # Returns true if the query contains words that this parser is
        # supposed to process
        # Else, returns false
        if "movie" in query:
            return True
        return False

    def process(self, query):
        # Processes the query using the self.ui.say(), self.ui.ask() and
        # self.ui.query_not_understood() methods.
        # Doesn't return a value
        movie_param = query.split()
        year = " ".join(movie_param[1:])

        try:
            url = "https://movies-tv-shows-database.p.rapidapi.com/"

            querystring = {"year":f"{year}","page":"1"}

            headers = {
                "Type": "get-popular-movies",
                "X-RapidAPI-Key": "3a30aa888cmsh26f4ffe26648224p114018jsn18c1949bae07",
                "X-RapidAPI-Host": "movies-tv-shows-database.p.rapidapi.com"
            }

            response = requests.get(url, headers=headers, params=querystring)
            data = response.json()

            self.ui.say(f"10 popularni filmova {data['movie_results'][1]['year']}")
            br = 0
            for i in range(min(10, len(data['movie_results']))):
                movie_title = data['movie_results'][i]['title']
                br += 1
                self.ui.say(f"{br}. - {movie_title}")
        except:
            print("error")