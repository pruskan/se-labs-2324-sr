from query_handler_base import QueryHandlerBase
#from pip._vendor import requests
import requests

class ChuckJokeHandler(QueryHandlerBase):
    def can_process_query(self, query):
        # Returns true if the query contains words that this parser is
        # supposed to process
        # Else, returns false
        if "chuck" in query:
            return True
        return False

    def process(self, query):
        # Processes the query using the self.ui.say(), self.ui.ask() and
        # self.ui.query_not_understood() methods.
        # Doesn't return a value

        tran_params = query.split()
        #result_string = " ".join(tran_params[1:])

        try:
            url = "https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random"

            headers = {
                "accept": "application/json",
                "X-RapidAPI-Key": "3a30aa888cmsh26f4ffe26648224p114018jsn18c1949bae07",
                "X-RapidAPI-Host": "matchilling-chuck-norris-jokes-v1.p.rapidapi.com"
            }

            response = requests.get(url, headers=headers)
            response = response.json()

            self.ui.say(response['value'])
        except:
            print("error")