from query_handler_base import QueryHandlerBase
#from pip._vendor import requests
import requests

class WeatherHandler(QueryHandlerBase):
    def can_process_query(self, query):
        # Returns true if the query contains words that this parser is
        # supposed to process
        # Else, returns false
        if "weather" in query:
            return True
        return False

    def process(self, query):
        # Processes the query using the self.ui.say(), self.ui.ask() and
        # self.ui.query_not_understood() methods.
        # Doesn't return a value

        weather_params = query.split()
        result_string = " ".join(weather_params[1:])

        try:
            url = "https://weatherapi-com.p.rapidapi.com/forecast.json"

            querystring = {"q":{result_string},"days":"3"}

            headers = {
                "X-RapidAPI-Key": "3a30aa888cmsh26f4ffe26648224p114018jsn18c1949bae07",
                "X-RapidAPI-Host": "weatherapi-com.p.rapidapi.com"
            }

            response = requests.get(url, headers=headers, params=querystring)
            data = response.json()

            print(data['current']['temp_c'])

            self.ui.say(f"U {data['location']['name']} je trenutno {data['current']['temp_c']}°C")
        except:
            print("Tvoj grad nije pronaden")